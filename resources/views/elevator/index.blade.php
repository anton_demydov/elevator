<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>
    <style>
        .active {
            background: blue;
        }

        .active.door_open {
            background: yellow;
        }
    </style>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <script>
        var last_called_floor = null;
        document.addEventListener("DOMContentLoaded", function () {

                    var api_call_url = '/api/call';
                    var api_move_url = '/api/move';
            var ws_url = '{{env('WS_CLIENT_URL')}}';

                    var elevatorConn = new WebSocket(ws_url);
                    elevatorConn.onopen = function (e) {
                        console.log('Connection established');
                    }
                    elevatorConn.error = function (e) {
                        console.log('Connection error');

                        document.querySelector('#elevators_table tbody').innerHTML = '<tr><td colspan="3" class="" height="50px">Please run <b>php artisan elevator:serve 1</b>command. (1 = delay in seconds) </td></tr>';
                    }

                    elevatorConn.onmessage = function (e) {

                        var data = JSON.parse(e.data);

                        if (data.floor_count != document.querySelectorAll('tr.floor').length) {
                            document.querySelector('#elevators_table tbody').innerHTML = renderElevators(data.floor_count, data.elevators);
                        } else {
                            updateElevators(data.floor_count, data.elevators);
                        }
                    }


                    var updateElevators = function (floors_count, elevators) {
                        var floors = document.querySelectorAll('td.elevator');
                        for (var f_index in floors) {
                            if (floors[f_index].classList) {
                                floors[f_index].classList.remove('active');
                                floors[f_index].classList.remove('door_open');
                            }

                        }
                        for (var floor = floors_count; floor >= 1; floor--) {
                            elevators.forEach(function (elevator) {
                                if (floor == elevator.current_floor) {
                                    document.querySelector('.elevator.elevator_' + elevator.id + '_floor_' + floor).classList.add('active')
                                    if (last_called_floor > 0) {
                                        document.querySelectorAll('.send_button').forEach(function (item) {
                                            item.removeAttribute('disabled');
                                        });
                                    } else {
                                        document.querySelectorAll('.send_button').forEach(function (item) {
                                            item.setAttribute('disabled', 'disabled');
                                        });
                                    }

                                    if (elevator.door == 1) {
                                        document.querySelector('.elevator.elevator_' + elevator.id + '_floor_' + floor).classList.add('door_open')

                                        last_called_floor = floor;
                                    }
                                    /*else {
                                     last_called_floor = 0;
                                     }*/
                                }
                            });
                        }
                    }


                    var renderElevators = function (floors_count, elevators) {

                        var html = '';
                        for (var floor = floors_count; floor >= 1; floor--) {
                            html += '<tr class="floor" ><td><b class="send_ floor_' + floor + '" ' + (last_called_floor > 0 ? '' : 'disabled') + ' data-value="' + floor + '">(' + floor + ')</input></td>';
                            elevators.forEach(function (elevator) {
                                var el_class = '';
                                if (floor == elevator.current_floor) {
                                    el_class += ' active';
                                    if (elevator.door == 1) {
                                        el_class += ' door_open';
                                        last_called_floor = floor;
                                    }
                                    /* else {
                                     last_called_floor = 0;
                                     }*/
                                }
                                html += '<td class="elevator ' + el_class + ' elevator_' + elevator.id + '_floor_' + floor + ' " width="50px" height="50px"></td>';

                            });

                            html += '<td class="call_elevator"><button class="call_elevator" ' + (last_called_floor == floor ? 'disabled' : '') + ' value="' + floor + '">Call</button></td>';

                            html += '</tr><tr>';
                        }
                        html += '</tr>';
                        return html;
                    }


                    document.querySelector("#elevators_table").addEventListener('click', function (e) {

                        var token = document.querySelector("input[name=_token]").value;

                        if (e.target.classList.contains('call_elevator')) {

                            var floor = e.target.value;

                            last_called_floor = floor;
                            var fd = new FormData();
                            fd.append('floor', floor);

                            var xhr = new XMLHttpRequest();

                            xhr.addEventListener('error', function (event) {
                                alert('Oups! Something went wrong.');
                            });
                            xhr.open("POST", api_call_url);

                            xhr.setRequestHeader('X-CSRF-TOKEN', token);

                            xhr.send(fd);
                        }

                    })
                    document.querySelector("#elevators_table").addEventListener('click', function (e) {

                        var token = document.querySelector("input[name=_token]").value;
                        if (last_called_floor > 0 && e.target.classList.contains('send_button')) {

                            var from = last_called_floor;
                            var to = e.target.value;
                            last_called_floor = to;

                            var fd = new FormData();
                            fd.append('from', from);
                            fd.append('to', to);

                            var xhr = new XMLHttpRequest();

                            xhr.addEventListener('error', function (event) {
                                alert('Oups! Something went wrong.');
                            });
                            xhr.open("POST", api_move_url);

                            xhr.setRequestHeader('X-CSRF-TOKEN', token);

                            xhr.send(fd);
                        }

                    });

            document.querySelector("#from_to").addEventListener('submit', function (e) {
                e.preventDefault();
                var token = document.querySelector("input[name=_token]").value;

                var fd = new FormData(e.target);

                var xhr = new XMLHttpRequest();

                xhr.addEventListener('error', function (event) {
                    alert('Oups! Something went wrong.');
                });
                xhr.open("POST", api_move_url);

                xhr.setRequestHeader('X-CSRF-TOKEN', token);

                xhr.send(fd);


                    })
                }
        )


    </script>

</head>
<body>
<div class="container">
    <div class="content">

        <table id="elevators_table" border="solid black 1px">
            <tbody>
            <tr class="floor">
                <td colspan="3" width="40px" height="50px"></td>
                <td width="50px" height="50px"></td>
                <td width="40px" height="50px"></td>
            </tr>
            </tbody>

        </table>
        <hr>
        <h2>Api call</h2>
        <form id="from_to" action="/api/move" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <label for="from">From:</label>
            <input type="number" name="from" value="" required>
            <label for="to">To:</label>

            <input type="number" name="to" value="" required>
            <input type="submit">
        </form>
    </div>
</div>
</body>
</html>
