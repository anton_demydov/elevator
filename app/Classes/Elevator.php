<?php

namespace App\Classes;

use Illuminate\Support\Facades\Log;


/**
 * The Elevator Test
 * ==========================================
 * First there is an elevator * class.
 * It has a direction (up, down, stand, maintenance), a current floor  and a list of floor requests sorted in the direction.
 *
 * Each elevator has a set of signals:
 *          Alarm, Door open, Door close
 *
 * The scheduling will be like:
 *          if available pick a standing elevator for this floor.
 *          else pick an elevator moving to this floor.
 *          else pick a standing elevator on another floor.
 * Sample data:
 *          - Elevator standing in first floor
 *          - Request from 6th floor go down to ground(first floor).
 *          - Request from 5th floor go up to 7th floor
 *          - Request from 3rd floor go down to ground
 *          - Request from ground go up to 7th floor.
 *          - Floor 2 and 4 are in maintenance.
 * Extra Point: Making an API to send/receive requests to elevator and write log file.
 * Class Elevator
 * @package App\Classes
 */
class Elevator
{
    const DIRECTION_UP = 'up';
    const DIRECTION_DOWN = 'down';
    const DIRECTION_STAND = 'stand';

    const SIGNAL_ALARM = 'Alarm';
    const SIGNAL_OPEN = 'Door open';
    const SIGNAL_CLOSE = 'Door close';

    const FLOOR_ERROR = 'bad floor number';
    const FLOOR_EMPTY = 'floor number not set';

    const DOOR_OPENED = 1;
    const DOOR_CLOSED = 0;
    const DOOR_TIMER = 3;

    const STATE_MAINTENANCE = 'maintenance';
    const STATE_AVAILABLE = 'available';
    const STATE_BUSY = 'busy';


    private $id = 0;

    private $current_floor = 1;

    private $destination_floor;

    private $direction = self::DIRECTION_STAND;

    private $last_event = self::SIGNAL_CLOSE;

    private $floor_list = [];

    private $floor_request_list = [];

    private $current_state;

    private $door_state = self::DOOR_CLOSED;

    private $door_timer_left = 0;


    /**
     * Elevator constructor.
     *
     * @param array $floors_list
     * @param int $current_floor
     * @param int $id
     */
    public function __construct(array $floors_list, $current_floor = 1, $id = 0)
    {
        $this->id = $id;
        $this->floor_list = $floors_list;
        $this->current_floor = $current_floor;
        $this->current_state = self::STATE_AVAILABLE;
        $this->direction = self::DIRECTION_STAND;
    }

    /**
     * Request to elevator moving
     *
     * @param int $floor_number
     */
    public function moveTo($floor_number)
    {
        Log::info('Elevator  '.$this->id.' got request to go ', ['floorNum' => $floor_number]);
        if ($floor_number) {
            if (in_array($floor_number, $this->floor_list)) {
                $this->floor_request_list[] = $floor_number;
            } else {
                $this->raiseEvent(self::FLOOR_ERROR);
            }
        } else {
            $this->raiseEvent(self::FLOOR_EMPTY);
        }
    }


    /**
     * Main moving function
     */
    public function move()
    {
        $this->doorUpdate();
        if ($this->door_state == self::DOOR_CLOSED) {
            Log::info('Elevator ' . $this->id . ' state:' . $this->current_state);
            if (!empty($this->floor_request_list) && !$this->destination_floor) {
                $this->destination_floor = array_shift($this->floor_request_list);
            }
            if ($this->destination_floor) {
                $this->current_state = self::STATE_BUSY;
                if ($this->destination_floor != $this->current_floor) {
                    if ($this->destination_floor < $this->current_floor) {
                        $this->moveDown();
                    } else {
                        $this->moveUp();
                    }
                } else {
                    $this->destination_floor = array_shift($this->floor_request_list);
                    if (in_array($this->direction, [self::DIRECTION_UP, self::DIRECTION_DOWN])) {
                        $this->direction = self::DIRECTION_STAND;
                        $this->raiseEvent(self::SIGNAL_OPEN);
                    } else {
                        if (self::SIGNAL_OPEN == $this->last_event) {
                            if (empty($this->floor_request_list)) {
                                $this->current_state = self::STATE_AVAILABLE;
                            }
                        } else {
                            $this->raiseEvent(self::SIGNAL_OPEN);
                        }
                    }
                }
            } else {
                $this->current_state = self::STATE_AVAILABLE;
            }
        }
    }

    /**
     * Raise elevator event
     *
     * @param $event_name
     */
    protected function raiseEvent($event_name)
    {
        $this->last_event = $event_name;
        switch ($event_name) {
            case self::SIGNAL_ALARM:
                Log::info('Elevator '.$this->id.': Alarm');
                break;
            case self::SIGNAL_OPEN:
                Log::info('Elevator '.$this->id.' signal to open door: ' . $this->id);
                $this->doorOpen();
                break;
            case self::SIGNAL_CLOSE:
                Log::info('Elevator '.$this->id.' signal to close door: ' . $this->id);

                $this->doorClose();
                break;
            default:
                Log::info('Elevator '.$this->id.'  other event: ' . $event_name);
                break;
        }

        Log::info('Elevator '.$this->id.' state : ' . $this->current_state);
    }

    /**
     * @return array
     */
    public function getFloorList()
    {
        return $this->floor_list;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getCurrentFloor()
    {
        return $this->current_floor;
    }

    /**
     * @return boolean
     */
    public function isDestinationFloor()
    {
        return $this->destination_floor;
    }

    /**
     * @return string
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @return string
     */
    public function getLastEvent()
    {
        return $this->last_event;
    }

    /**
     * @return array
     */
    public function getFloorRequestList()
    {
        return $this->floor_request_list;
    }

    /**
     * @return string
     */
    public function getCurrentState()
    {
        return $this->current_state;
    }
    protected function doorUpdate()
    {
        Log::info('Elevator ' . $this->id . ' doors timer: ' . $this->door_timer_left);
        if ($this->door_timer_left > 0) {
            $this->door_timer_left--;
        } else {
            $this->raiseEvent(self::SIGNAL_CLOSE);
        }
    }
    public function doorOpen()
    {
//        $this->destination_floor = null;
        $this->door_state = self::DOOR_OPENED;
        $this->door_timer_left = self::DOOR_TIMER;
    }

    public function doorClose()
    {
        $this->door_state = self::DOOR_CLOSED;
        $this->door_timer_left = 0;
    }


    public function getDoorState()
    {
        return $this->door_state;
    }

    public function getDestinationFloor()
    {
        return $this->destination_floor;
    }

    public function moveUp()
    {
        $this->current_floor++;
        $this->direction = self::DIRECTION_UP;
        Log::info('Elevator ' . $this->id . ' move up');
    }

    public function moveDown()
    {
        $this->current_floor--;
        $this->direction = self::DIRECTION_DOWN;
        Log::info('Elevator ' . $this->id . ' move down');

    }
}