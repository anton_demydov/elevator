<?php
namespace App\Classes;

use Illuminate\Support\Facades\Log;


/**
 * Class ElevatorController
 *
 * @package elevator
 */
class ElevatorsDispatcher
{
    /**
     * @var Elevator[]
     */
    protected $elevators_list = [];

    /**
     * @var int
     */
    protected $floor_count = 12;

    /**
     * @var int
     */
    protected $elevator_count = 1;

    /**
     * ElevatorController constructor.
     *
     * @param int $floor_count
     * @param int $elevator_count
     */
    public function __construct($floor_count = 12, $elevator_count = 1)
    {
        $this->floor_count = $floor_count;
        $this->elevator_count = $elevator_count;
        $this->initElevators();
    }

    /**
     * Init elevators
     */
    private function initElevators()
    {
        $floors_list = range(1, $this->floor_count);

        for ($i = 1; $i <= $this->elevator_count; $i++) {
            $this->elevators_list[] = new Elevator($floors_list, intval(rand(1, $this->floor_count)), $i);
        }
    }

    /**
     * Process elevator request
     *
     * @param $from
     * @param $to
     *
     * @return bool|Elevator
     */
    public function elevatorRequest($from, $to = 0)
    {
        Log::info('Elevator request button', ['from' => $from, 'to' => $to]);

        if ($elevator = $this->getClosestElevator($from)) {
            Log::info('Sending  closest elevator', ['elevator' => $elevator, 'from' => $from, 'to' => $to]);
            if ($elevator->getCurrentState() == Elevator::STATE_AVAILABLE && $elevator->getCurrentFloor() == $from) {
                $elevator->doorOpen();
            } else {
                $elevator->moveTo($from);
            }

            if ($to) {
                $elevator->moveTo($to);
            }
            return $elevator;
        } else {
            Log::info('Can\'t find elevator', ['from' => $from, 'to' => $to]);
            return false;
        }
    }

    public function callElevator($to)
    {
        if ($elevator = $this->getClosestElevator($to)) {
            if ($elevator == $to) {
                $elevator->doorOpen();
            } else {
                $elevator->moveTo($to);
            }
            return $elevator;
        } else {
            return false;
        }
    }


    /**
     * Return closest elevator
     * The scheduling will be like:
     *          if available pick a standing elevator for this floor.
     *          else pick an elevator moving to this floor.
     *          else pick a standing elevator on another floor.
     * @param $floor_number
     *
     * @return bool|Elevator
     */

    private function getClosestElevator($floor_number)
    {
        $elevator_distance = [];
        foreach ($this->elevators_list as $index => $elevator) {
            /** @var $elevator Elevator */
            if (Elevator::STATE_AVAILABLE == $elevator->getCurrentState() && $elevator->getCurrentFloor() == $floor_number) {
                return $elevator;
            } elseif ($elevator->getDestinationFloor() == $floor_number) {
                return $elevator;
            } else {
                if (Elevator::STATE_AVAILABLE == $elevator->getCurrentState()) {
                    $distance = $elevator->getCurrentFloor() - $floor_number;
                    if ($distance < 0) {
                        $distance *= -1;
                    }
                    $elevator_distance[$index] = $distance;
                } elseif (Elevator::STATE_BUSY) {
                    $distance = $elevator->getDestinationFloor() - $floor_number;
                    if ($distance < 0) {
                        $distance *= -1;
                    }
                    $elevator_distance_busy[$index] = $distance;
                } else {
                    //maintenance
                }

            }
        }

        if (!empty($elevator_distance)) {
            $item = array_keys($elevator_distance, min($elevator_distance));
            return isset($this->elevators_list[$item[0]]) ? $this->elevators_list[$item[0]] : false;
        } elseif ($elevator_distance_busy) {
            $item_busy = array_keys($elevator_distance_busy, min($elevator_distance));
            return isset($this->elevators_list[$item_busy[0]]) ? $this->elevators_list[$item_busy[0]] : false;

        } else {
            return false;
        }
    }

    /**
     * Run elevator moving
     */
    public function elevatorRun()
    {
        foreach ($this->elevators_list as $elevator) {
            $elevator->move();
        }
        return $this->currentState();
    }

    /**
     * Display current system state
     */
    private function currentState()
    {

        return $this->elevators_list;
    }

    /**
     * @return Elevator[]
     */
    public function getElevatorsList()
    {
        return $this->elevators_list;
    }

    /**
     * @return Elevator[]
     */
    public function getElevatorsData()
    {
        $elevators = [];
        foreach ($this->getElevatorsList() as $item) {
            $elevators[] = [
                'id' => $item->getId(),
                'current_floor' => $item->getCurrentFloor(),
                'destenation_floor' => $item->getDestinationFloor(),
                'floor_request_list' => $item->getFloorRequestList(),
                'door' => $item->getDoorState(),
                'current_state' => $item->getCurrentState(),
            ];
        }

        return ['floor_count' => $this->floor_count, 'elevators' => $elevators];
    }


}