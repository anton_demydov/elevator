<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', ['as' => 'elevator', 'uses' => 'ElevatorController@index']);

Route::post('api/call', ['as' => 'api.move', 'uses' => 'ElevatorApiController@call']);
Route::post('api/move', ['as' => 'api.call', 'uses' => 'ElevatorApiController@move']);
