<?php

namespace App\Http\Controllers;


use App\Events\ElevatorCall;
use App\Events\ElevatorMove;
use Illuminate\Http\Request;


class ElevatorApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * move
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function call(Request $request)
    {
        $floor_number = (int)$request->input('floor');
        if ($floor_number) {
            event(new ElevatorCall($floor_number));
        }
        return ['floor' => $floor_number];
    }

    public function move(Request $request)
    {

        $from = (int)$request->input('from');
        $to = (int)$request->input('to');
        if ($from && $to) {
            event(new ElevatorMove($from, $to));
        }

        return ['from' => $from, 'to' => $to];
    }

}
