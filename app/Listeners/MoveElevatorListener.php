<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Log;
use WebSocket\Client;
use App\Events\ElevatorMove;


class MoveElevatorListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ElevatorMove  $event
     * @return void
     */
    public function handle(ElevatorMove $event)
    {
        Log::info('Elevator move ',['from'=>$event->from,'to'=>$event->to]);

        $client = new Client(env('WS_SERVER_URL','ws://127.0.0.1:8081/'));

        $client->send(json_encode(['from'=>$event->from,'to'=>$event->to]));
    }
}
