<?php

namespace App\Listeners;

use App\Events\ElevatorCall;
use WebSocket\Client;
use Illuminate\Support\Facades\Log;

class CallElevatorListener
{


    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }


    /**
     * Handle the event.
     *
     * @param  ElevatorCall  $event
     * @return void
     */
    public function handle(ElevatorCall $event)
    {
        Log::info('Elevator call button',['floor_number'=>$event->floor_number]);

        $client = new Client(env('WS_SERVER_URL','ws://127.0.0.1:8081/'));

        $client->send(json_encode(['floor'=>$event->floor_number]));
    }

}
