<?php

namespace App\Console\Commands;

use App\Classes\Elevator;
use App\Classes\ElevatorsDispatcher;
use Devristo\Phpws\Server\WebSocketServer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class LiveElevator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'elevator:serve {delay} {--elevators=} {--floors=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs elevator WebSocket live server';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $delay = $this->argument('delay');
        $elevators_number = $this->option('elevators');
        $floors_number = $this->option('floors');
        $loop = \React\EventLoop\Factory::create();

        $logger = new \Zend\Log\Logger();
        $logger->addWriter(new \Zend\Log\Writer\Stream("php://output"));

        $elevatorDispatcher = new ElevatorsDispatcher($floors_number, $elevators_number);
        $server = new WebSocketServer(env('WS_SERVER_URL', "ws://127.0.0.1:8081"), $loop, $logger);
        $server->on('message', function ($conn, $msg) use ($server, $elevatorDispatcher, $logger) {

            $logger->notice('income data: ' . $msg->getData());

            $data = json_decode($msg->getData());

            if ($data && isset($data->floor)) {
                $elevatorDispatcher->elevatorRequest($data->floor);
            } elseif ($data && isset($data->from) && isset($data->to)) {
                $logger->notice('Server requested moveElevator :' . $msg->getData());
                $elevatorDispatcher->elevatorRequest($data->from, $data->to);
            } else {
                $logger->notice("Data parse err : " . $msg->getData());
            }

        });

        $loop->addPeriodicTimer($delay, function () use ($server, $logger, $elevatorDispatcher) {
            $elevatorDispatcher->elevatorRun();
            $elevators = $elevatorDispatcher->getElevatorsData();
            $logger->notice("Broadcasting elevators data: " . json_encode($elevators));
            foreach ($server->getConnections() as $client) {
                $client->sendString(json_encode($elevators));
            }
        });
        $server->bind();
        $loop->run();
    }
}
