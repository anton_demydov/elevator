<?php

namespace App\Events;

class ElevatorMove extends Event
{

    public $from;
    public $to;


    /**
     * Create a new event instance. ElevatorMove constructor.
     * @param $from
     * @param $to
     */
    public function __construct($from, $to)
    {

        $this->from = $from;
        $this->to = $to;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
