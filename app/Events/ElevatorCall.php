<?php

namespace App\Events;
use Illuminate\Support\Facades\Log;


class ElevatorCall extends Event
{

    public $floor_number;

    public function __construct($floor_number)
    {
        Log::info('Elevator call button',['floor'=>$floor_number]);

        $this->floor_number = $floor_number;
    }

    public function broadcastOn()
    {
        return [];
    }
}
